package ma.octo.assignement.service;
import ma.octo.assignement.dto.OperationDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.model.util.OperationType;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.BanqueService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;



import java.math.BigDecimal;
import java.util.Date;
@SpringBootTest
class OperationServiceTest {


    @Autowired
    private BanqueService banqueService;
    @Autowired
    private CompteRepository compteRepository;

    @Test
    void versementoperation() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {

        BigDecimal accMonth=compteRepository.findByRib("RIB1").getSolde();
        OperationDto operationDto =new OperationDto();
        operationDto.setDateExecution(new Date());
        operationDto.setMontant(new BigDecimal(50));
        operationDto.setType(OperationType.VERSEMENT.getType());
        operationDto.setMotif("test");
        operationDto.setRibCompteBeneficiaire("RIB1");
        banqueService.banqueprocessus(operationDto);
        assertEquals(compteRepository.findByRib("RIB1").getSolde(),accMonth.add(new BigDecimal(50)));
    }

    @Test
    void retraitoperation() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {

        BigDecimal accMonth=compteRepository.findByRib("RIB1").getSolde();
        OperationDto operationDto =new OperationDto();
        operationDto.setDateExecution(new Date());
        operationDto.setMontant(new BigDecimal(10));
        operationDto.setType(OperationType.RETRAIT.getType());
        operationDto.setMotif("test");
        operationDto.setRibCompteEmetteur("RIB1");
        banqueService.banqueprocessus(operationDto);
        assertEquals(compteRepository.findByRib("RIB1").getSolde(),accMonth.subtract(new BigDecimal(10)));
    }


    @Test
    void operationLimitExeception() throws TransactionException, CompteNonExistantException {
        OperationDto operationDto =new OperationDto();
        operationDto.setDateExecution(new Date());
        operationDto.setMontant(new BigDecimal(100000));
        operationDto.setType(OperationType.VERSEMENT.getType());
        operationDto.setMotif("test");
        operationDto.setRibCompteBeneficiaire("RIB1");
            assertThrows(TransactionException.class, () -> {banqueService.banqueprocessus(operationDto); });
    }

    @Test
    void operationCompteNotExsistExeception() throws TransactionException, CompteNonExistantException {
        OperationDto operationDto =new OperationDto();
        operationDto.setDateExecution(new Date());
        operationDto.setMontant(new BigDecimal(10));
        operationDto.setType(OperationType.VERSEMENT.getType());
        operationDto.setMotif("test");
        operationDto.setRibCompteEmetteur("RIB78");
        assertThrows(CompteNonExistantException.class, () -> {banqueService.banqueprocessus(operationDto); });
    }

}

