package ma.octo.assignement.service;

import ma.octo.assignement.dto.OperationDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.model.*;
import ma.octo.assignement.model.util.OperationType;
import ma.octo.assignement.repository.AuditOperationRepository;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.OperationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class BanqueService {

    final CompteRepository compteRepository;
    final OperationRepository operationRepository;
    final AuditOperationRepository auditOperationRepository;
    public static final int MONTANT_MAXIMAL = 10000;
    Logger LOGGER = LoggerFactory.getLogger(BanqueService.class);


    public BanqueService(CompteRepository compteRepository, OperationRepository operationRepository, AuditOperationRepository auditOperationRepository) {
        this.compteRepository = compteRepository;
        this.operationRepository = operationRepository;
        this.auditOperationRepository = auditOperationRepository;
    }


    public void banqueprocessus(OperationDto operationDto) throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {

        OperationType operationType=null;
        AuditOperation auditOperation = new AuditOperation();

        if(operationDto.getType().equals(operationType.VERSEMENT.getType())) {

            auditOperation.setMessage("versement vers " + operationDto.getRibCompteBeneficiaire()
                    + " d'un montant de " + operationDto.getMontant()
                    .toString());
            auditOperation.setEventType(OperationType.VERSEMENT);
            versement(operationDto,auditOperation);

        }
        else if(operationDto.getType().equals(operationType.RETRAIT.getType())) {
            auditOperation.setMessage("retrait depuis " + operationDto.getRibCompteEmetteur() + " d'un montant de " +  operationDto.getMontant()
                    .toString());
            auditOperation.setEventType(OperationType.RETRAIT);
            retrait(operationDto,auditOperation);
        }
        else {

            auditOperation.setMessage("Virement depuis " + operationDto.getRibCompteEmetteur() + " vers " + operationDto
                    .getRibCompteBeneficiaire()+ " d'un montant de " + operationDto.getMontant()
                    .toString());
            auditOperation.setEventType(OperationType.VIREMENT);
            virement(operationDto,auditOperation);
        }
        LOGGER.info("Audit de l'événement {}", auditOperation.getMessage());



    }
    public void retrait(OperationDto operationDto,AuditOperation auditOperation) throws CompteNonExistantException, SoldeDisponibleInsuffisantException, TransactionException {
        Compte compte = compteRepository.findByRib(operationDto.getRibCompteEmetteur());

        if(operationDto.getMontant().intValue()> MONTANT_MAXIMAL)throw new TransactionException("vous avez depasse les limites dépasser");
        if(compte == null) throw new CompteNonExistantException();
        if(compte.getSolde().compareTo(operationDto.getMontant()) < 0) throw new SoldeDisponibleInsuffisantException();

        Retrait retrait = new Retrait(new Date(),operationDto.getMontant(), compte,operationDto.getMotif());
        retrait.setAuditOperation(auditOperation);
        compte.setSolde(compte.getSolde().subtract(operationDto.getMontant()));
        auditOperationRepository.save(auditOperation);
        operationRepository.save(retrait);
        compteRepository.save(compte);

    }

    public void versement(OperationDto operationDto,AuditOperation auditOperation) throws CompteNonExistantException, TransactionException {
        Compte compte = compteRepository.findByRib(operationDto.getRibCompteBeneficiaire());
        if(operationDto.getMontant().intValue()> MONTANT_MAXIMAL)throw new TransactionException("vous avez depasse les limites dépasser");
        if(compte == null) throw new CompteNonExistantException();

        Versement versement = new Versement(new Date(), operationDto.getMontant(), compte,operationDto.getMotif());
        versement.setAuditOperation(auditOperation);
        compte.setSolde(compte.getSolde().add(operationDto.getMontant()));
        auditOperationRepository.save(auditOperation);
        operationRepository.save(versement);
        compteRepository.save(compte);
    }

    public void virement(OperationDto operationDto,AuditOperation auditOperation) throws CompteNonExistantException, SoldeDisponibleInsuffisantException, TransactionException {
        Compte compteEmetteur = compteRepository.findByRib(operationDto.getRibCompteEmetteur());
        Compte compteRecepteur = compteRepository.findByRib(operationDto.getRibCompteBeneficiaire());

        if(compteEmetteur == null || compteRecepteur ==null) throw new CompteNonExistantException("Le compte  n'exsite pas!");
        if(compteEmetteur.equals(compteRecepteur)) {
            throw new TransactionException("Le virement d'un compte vers ce même compte n'est pas autorisé!");
        }



        retrait(operationDto ,auditOperation);
        versement(operationDto ,auditOperation);

    }


}
