package ma.octo.assignement.controller;

import ma.octo.assignement.dto.OperationDto;
import ma.octo.assignement.model.Compte;
import ma.octo.assignement.model.Operation;
import ma.octo.assignement.model.Utilisateur;
import ma.octo.assignement.model.util.OperationType;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.OperationRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.BanqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/v1/api/banques")

class BanqueController {

    private final CompteRepository compteRepository;
    private final OperationRepository operationRepository ;
    private final UtilisateurRepository utilisateurRepository;
    private final BanqueService banqueService;

    public BanqueController(CompteRepository compteRepository, OperationRepository operationRepository, UtilisateurRepository utilisateurRepository, BanqueService banqueService) {
        this.compteRepository = compteRepository;
        this.operationRepository = operationRepository;
        this.utilisateurRepository = utilisateurRepository;
        this.banqueService = banqueService;
    }


    @GetMapping("/users")
    ResponseEntity<List<Utilisateur>> loadAllUtilisateur() {
        List<Utilisateur> all = utilisateurRepository.findAll();
        return new ResponseEntity<List<Utilisateur>>(all , HttpStatus.OK);
    }


    @GetMapping("/operations")
    ResponseEntity<List<Operation>> loadAll() {
        List<Operation> all = operationRepository.findAll();
        return new ResponseEntity<List<Operation>>(all , HttpStatus.OK);
    }

    @GetMapping("/comptes")
    List<Compte> loadAllCompte() {

        List<Compte> all = compteRepository.findAll();
        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }

    }

    @RequestMapping(value="/operations/save",method=RequestMethod.POST)
    public String saveOperation(@RequestBody OperationDto operationDto) {

        try {
            banqueService.banqueprocessus(operationDto);
        }catch(Exception e) {
            return e.getMessage();
        }

        return "redirect:/consulterCompte?codeCompte="+operationDto.getNomEmetteur();
    }
}
