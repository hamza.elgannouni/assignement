package ma.octo.assignement.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
@Data
public class OperationDto {

  private BigDecimal montant;
  private String type;
  private Date dateExecution;
  private String nomEmetteur;
  private String prenomEmetteur;
  private String RibCompteEmetteur;
  private String RibCompteBeneficiaire;
  private String motif;



}
