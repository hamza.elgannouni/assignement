package ma.octo.assignement.repository;

import ma.octo.assignement.model.Retrait;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RetraitRepository extends JpaRepository<Retrait, Long> {
}
