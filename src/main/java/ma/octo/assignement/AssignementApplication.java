package ma.octo.assignement;

import ma.octo.assignement.dto.OperationDto;
import ma.octo.assignement.model.*;
import ma.octo.assignement.model.util.OperationType;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.RetraitRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.BanqueService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.math.BigDecimal;
import java.util.Date;


@EnableJpaAuditing
@SpringBootApplication
public class AssignementApplication implements CommandLineRunner {
	private final CompteRepository compteRepository;
	private final UtilisateurRepository utilisateurRepository;
	private final RetraitRepository retraitRepository;
	private final VersementRepository versementRepository;
	private final BanqueService banqueService;



	public AssignementApplication(CompteRepository compteRepository, UtilisateurRepository utilisateurRepository, RetraitRepository retraitRepository, VersementRepository versementRepository, BanqueService banqueService) {
		this.compteRepository = compteRepository;
		this.utilisateurRepository = utilisateurRepository;
		this.retraitRepository = retraitRepository;
		this.versementRepository = versementRepository;
		this.banqueService = banqueService;
	}

	public static void main(String[] args) {
		SpringApplication.run(AssignementApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Male");
		utilisateur1.setBirthdate(new Date());

		utilisateurRepository.save(utilisateur1);


		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");
		utilisateur2.setBirthdate(new Date());

		utilisateurRepository.save(utilisateur2);


		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		compteRepository.save(compte1);

		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);

		compteRepository.save(compte2);
		OperationDto operationDto = new OperationDto();
		operationDto.setMotif("test");
		operationDto.setMontant(new BigDecimal(200));
		operationDto.setType(OperationType.VERSEMENT.getType());
		operationDto.setRibCompteBeneficiaire("RIB2");
		operationDto.setDateExecution(new Date());
		banqueService.banqueprocessus(operationDto);

		operationDto.setType(OperationType.RETRAIT.getType());
		operationDto.setRibCompteEmetteur("RIB2");
		banqueService.banqueprocessus(operationDto);

		operationDto.setType(OperationType.VIREMENT.getType());
		operationDto.setMontant(new BigDecimal(500));
		operationDto.setRibCompteEmetteur("RIB1");
		operationDto.setRibCompteBeneficiaire("RIB2");
		operationDto.setDateExecution(new Date());
		operationDto.setMotif("test 2020");
		banqueService.banqueprocessus(operationDto);








	}
}
