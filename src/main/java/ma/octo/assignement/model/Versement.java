package ma.octo.assignement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class Versement extends Operation {

    public Versement() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Versement(Date dateOperation, BigDecimal montant, Compte compte,String motif) {
        super(dateOperation, montant, compte,motif);
        // TODO Auto-generated constructor stub
    }

}
