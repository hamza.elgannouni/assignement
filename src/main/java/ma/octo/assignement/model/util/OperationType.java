package ma.octo.assignement.model.util;

public enum OperationType {

  VIREMENT("virement"),
  RETRAIT("retrait"),
  VERSEMENT("Versement");

  private String type;

  OperationType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
