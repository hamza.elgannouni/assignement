package ma.octo.assignement.model;

import lombok.Data;
import ma.octo.assignement.model.util.OperationType;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "AUDIT_OPERATION")
public class AuditOperation  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 100)
    private String message;

    @Enumerated(EnumType.STRING)
    private OperationType eventType;






}
