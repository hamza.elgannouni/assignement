package ma.octo.assignement.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table
public class Retrait extends  Operation  {
    public Retrait() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Retrait(Date dateOperation, BigDecimal montant, Compte compte,String motif) {
        super(dateOperation, montant, compte,motif);
        // TODO Auto-generated constructor stub
    }

}
