
package ma.octo.assignement.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@EntityListeners(AuditingEntityListener.class)
@Data
@Entity
@Table
public class Operation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(precision = 16, scale = 2, nullable = false)
    private BigDecimal montant;
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateExecution;
    @Column(length = 200)
    private String motif;
    @ManyToOne()
    private Compte compte;
    @OneToOne
    @JoinColumn(name = "Audit")
    private AuditOperation auditOperation;
    @Column(name = "created_date", nullable = false, updatable = false)
    @CreatedDate
    private long createdDate;
    @Column(name = "modified_date")
    @LastModifiedDate
    private long modifiedDate;

    public Operation() {
    }

    public Operation(Date dateOperation, BigDecimal montant, Compte compte,String motif) {
        this.dateExecution = dateOperation;
        this.montant = montant;
        this.compte = compte;
        this.motif =motif;
    }


}
