package ma.octo.assignement.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "COMPTE")
@EntityListeners(AuditingEntityListener.class)
public class Compte {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(length = 16, unique = true)
    private String nrCompte;
    private String rib;
    @Column(precision = 16, scale = 2)
    private BigDecimal solde;
    @ManyToOne
    private Utilisateur utilisateur;
    @OneToMany(fetch = FetchType.LAZY)
    private List<Operation> operations;
    @Column(name = "created_date", nullable = false, updatable = false)
    @CreatedDate
    private long createdDate;
    @Column(name = "modified_date")
    @LastModifiedDate
    private long modifiedDate;

    public Compte() {
    }

    public Compte(Long id, String nrCompte, String rib, BigDecimal solde, Utilisateur utilisateur, List<Operation> operations, long createdDate, long modifiedDate) {
        this.id = id;
        this.nrCompte = nrCompte;
        this.rib = rib;
        this.solde = solde;
        this.utilisateur = utilisateur;
        this.operations = operations;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNrCompte(String nrCompte) {
        this.nrCompte = nrCompte;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public void setSolde(BigDecimal solde) {
        this.solde = solde;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public void setModifiedDate(long modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Long getId() {
        return id;
    }

    public String getNrCompte() {
        return nrCompte;
    }

    public String getRib() {
        return rib;
    }

    public BigDecimal getSolde() {
        return solde;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public long getModifiedDate() {
        return modifiedDate;
    }
}
